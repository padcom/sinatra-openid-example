require 'rubygems'
require 'sinatra'
require 'rack/openid'

use Rack::Session::Cookie
use Rack::OpenID

helpers do
  def logged_in
    session[:openid] && session[:openid].status == :success
  end
end

before do
  session[:openid] = env["rack.openid.response"] if env["rack.openid.response"]
end

get '/' do
  haml :index
end

post %r{/login(.*)} do
  if logged_in
    redirect '/'
  else
    [ 401, { 'WWW-Authenticate' => Rack::OpenID.build_header(:identifier => params[:openid_identifier]) }, [] ]
  end
end

get '/logout' do
  session.clear
  redirect '/'
end

__END__

@@ index

%a{ :href => '/logout' } Logout

- if logged_in
  %pre Logged in! <br/> #{ session[:openid].to_yaml }
- else
  %pre Not logged in
  %form{ :action => '/login', :method => 'post' }
    %label{ :for => 'openid_identifier' } OpenID Identifier:
    %input{ :type => 'text', :name => 'openid_identifier', :value => 'https://www.google.com/accounts/o8/id' }
    %input{ :type => 'submit', :value => 'Login' }
